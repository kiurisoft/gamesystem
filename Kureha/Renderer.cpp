#include "Renderer.h"

namespace ku = GameSystem::Kureha;

ku::Renderer::Renderer(SDL_Window* win) :ren(nullptr){
	ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED);
	if (ren == nullptr){
		LogSystem::Log << "Rendererの初期化に失敗" << std::endl;
		LogSystem::Log << SDL_GetError() << std::endl;
	}
}

ku::Renderer::~Renderer(){
	if (ren != nullptr){
		SDL_DestroyRenderer(ren);
	}
}

int ku::Renderer::GetNumRenderDrivers(){
	auto comp = SDL_GetNumRenderDrivers();
	if (comp < 1){
		LogSystem::Log << SDL_GetError() << std::endl;
	}
	return comp;
}

void ku::Renderer::GetRenderDrawBlendMode(SDL_BlendMode* blendmode){
	if (SDL_GetRenderDrawBlendMode(ren, blendmode) != 0){
		LogSystem::Log << SDL_GetError() << std::endl;
	}
}

void ku::Renderer::GetRenderDrawColor(Uint8* r, Uint8* g, Uint8* b, Uint8* a){
	if (SDL_GetRenderDrawColor(ren, r, g, b, a) != 0){
		LogSystem::Log << SDL_GetError() << std::endl;
	}
}

SDL_Texture* ku::Renderer::GetRenderTarget(){
	return SDL_GetRenderTarget(ren);
}

void ku::Renderer::GetRendererOutputSize(int* w, int* h){
	if (SDL_GetRendererOutputSize(ren, w, h) != 0){
		LogSystem::Log << SDL_GetError() << std::endl;
	}
}

void ku::Renderer::RenderClear(){
	if (SDL_RenderClear(ren) != 0){
		LogSystem::Log << SDL_GetError() << std::endl;
	}
}

void ku::Renderer::RenderCopy(Texture& texture, const SDL_Rect* src, const SDL_Rect* dst){
	if (SDL_RenderCopy(ren, texture.get(), src, dst) != 0){
		LogSystem::Log << SDL_GetError() << std::endl;
	}
}

void ku::Renderer::RenderCopyEx(Texture& texture, const SDL_Rect* src, const SDL_Rect* dst,
	const double angle, const SDL_Point* center, const SDL_RendererFlip flip){
	if (SDL_RenderCopyEx(ren, texture.get(), src, dst, angle, center, flip) != 0){
		LogSystem::Log << SDL_GetError() << std::endl;
	}
}

void ku::Renderer::RenderDrawLine(int x1, int y1, int x2, int y2){
	if (SDL_RenderDrawLine(ren, x1, y1, x2, y2) != 0){
		LogSystem::Log << SDL_GetError() << std::endl;
	}
}

void ku::Renderer::RenderDrawLines(const SDL_Point* points, int count){
	if (SDL_RenderDrawLines(ren, points, count) != 0){
		LogSystem::Log << SDL_GetError() << std::endl;
	}
}

void ku::Renderer::RenderDrawPoint(int x, int y){
	if (SDL_RenderDrawPoint(ren, x, y) != 0){
		LogSystem::Log << SDL_GetError() << std::endl;
	}
}

void ku::Renderer::RenderDrawPoints(const SDL_Point* points, int count){
	if (SDL_RenderDrawPoints(ren, points, count) != 0){
		LogSystem::Log << SDL_GetError() << std::endl;
	}
}

void ku::Renderer::RenderDrawRect(const SDL_Rect* rect){
	if (SDL_RenderDrawRect(ren, rect) != 0){
		LogSystem::Log << SDL_GetError() << std::endl;
	}
}

void ku::Renderer::RenderDrawRects(const SDL_Rect* rects, int count){
	if (SDL_RenderDrawRects(ren, rects, count) != 0){
		LogSystem::Log << SDL_GetError() << std::endl;
	}
}

void ku::Renderer::RenderFillRect(const SDL_Rect* rect){
	if (SDL_RenderFillRect(ren, rect) != 0){
		LogSystem::Log << SDL_GetError() << std::endl;
	}
}

void ku::Renderer::RenderFillRects(const SDL_Rect* rects, int count){
	if (SDL_RenderFillRects(ren, rects, count) != 0){
		LogSystem::Log << SDL_GetError() << std::endl;
	}
}

void ku::Renderer::RenderGetClipRect(SDL_Rect* rect){
	SDL_RenderGetClipRect(ren, rect);
}

void ku::Renderer::RenderGetLogicalSize(int* w, int* h){
	SDL_RenderGetLogicalSize(ren, w, h);
}

void ku::Renderer::RenderGetScale(float* scaleX, float* scaleY){
	SDL_RenderGetScale(ren, scaleX, scaleY);
}

void ku::Renderer::RenderGetViewport(SDL_Rect* rect){
	SDL_RenderGetViewport(ren, rect);
}

void ku::Renderer::RenderPresent(){
	SDL_RenderPresent(ren);
}

void ku::Renderer::RenderSetClipRect(const SDL_Rect* rect){
	if (SDL_RenderSetClipRect(ren, rect) != 0){
		LogSystem::Log << SDL_GetError() << std::endl;
	}
}

void ku::Renderer::RenderSetLogicalSize(int w, int h){
	if (SDL_RenderSetLogicalSize(ren, w, h) != 0){
		LogSystem::Log << SDL_GetError() << std::endl;
	}
}

void ku::Renderer::RenderSetScale(float scaleX, float scaleY){
	if (SDL_RenderSetScale(ren, scaleX, scaleY) != 0){
		LogSystem::Log << SDL_GetError() << std::endl;
	}
}

void ku::Renderer::RenderSetViewport(const SDL_Rect* rect){
	if (SDL_RenderSetViewport(ren, rect) != 0){
		LogSystem::Log << SDL_GetError() << std::endl;
	}
}

void ku::Renderer::SetRenderDrawBlendMode(SDL_BlendMode blendmode){
	if (SDL_SetRenderDrawBlendMode(ren, blendmode) != 0){
		LogSystem::Log << SDL_GetError() << std::endl;
	}
}

void ku::Renderer::SetRenderDrawColor(Uint8 r, Uint8 g, Uint8 b, Uint8 a){
	if (SDL_SetRenderDrawColor(ren, r, g, b, a) != 0){
		LogSystem::Log << SDL_GetError() << std::endl;
	}
}

void ku::Renderer::SetRenderTarget(Texture& texture){
	if (SDL_SetRenderTarget(ren, texture.get()) != 0){
		LogSystem::Log << SDL_GetError() << std::endl;
	}
}