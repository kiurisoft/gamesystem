#pragma once

#include <SDL2\SDL.h>
#include <LogSystem\Log.h>

namespace GameSystem{
	namespace Kureha{
		class Window{
		private:
			SDL_Window* win;
			SDL_Renderer* ren;

			Window(const Window&);
			Window& operator=(const Window&);
		public:
			Window(const char* title, int x, int y, int w, int h, Uint32 flags = 0);
			~Window();

			SDL_Window* get();

			//wraping functions
			void DisableScreenSaver();
			void EnableScreenSaver();

			/*
			void GetClosestDisplayMode(int disp_index, const SDL_DisplayMode* mode, SDL_DisplayMode* closest);
			void GetCurrentDisplayMode(int disp_index, SDL_DisplayMode* cur);
			void GetDesktopDisplayMode(int disp_index, SDL_DisplayMode* cur);
			int GetDisplayMode(int disp_index, int mode_index, SDL_DisplayMode* mode);
			*/

			const char* GetCurrentVideoDriver();
			void GetDisplayBounds(int disp_index, SDL_Rect* rect);
			const char* GetDisplayName(int disp_index);
			int GetNumDisplayModes(int disp_index);
			int GetNumVideoDisplays();
			int GetNumVideoDrivers();
			const char* GetVideoDriver(int index);

			float GetWindowBrightness();
			//void GetWindowData();
			int GetWindowDisplayIndex();
			//int GetWindowDisplayMode(SDL_DisplayMode* mode);
			Uint32 GetWindowFlags();
			//SDL_Window* GetWindowFromID(Uint32 id);
			void GetWindowGammaRamp(Uint16* r, Uint16* g, Uint16* b);
			SDL_bool GetWindowGrab();
			int GetWindowID();
			void GetWindowMaximumSize(int* w, int* h);
			void GetWindowMinimumSize(int* w, int* h);
			Uint32 GetWindowPixelFormat();
			void GetWindowPosition(int* x, int* y);
			void GetWindowSize(int* w, int* h);

			//SDL_Surface* GetWindowSurface();
			const char* GetWindowTitle();
			//void GetWindowWMInfo(SDL_SysWMinfo* info)
			void HideWindow();
			SDL_bool IsScreenSaverEnabled();
			void MaximizeWindow();
			void MinimizeWindow();
			void RaiseWindow();
			void RestoreWindow();
			void SetWindowBordered(SDL_bool bordered);
			void SetWindowBrightness(float brightness);
			//SetWindowData
			//SetWindowDisplayMode
			void SetWindowFullscreen(Uint32 flags);
			void SetWindowGammaRamp(const Uint16* r, const Uint16* g, const Uint16* b);
			void SetWindowGrab(SDL_bool grabbed);
			void SetWindowIcon(SDL_Surface* icon);
			void SetWindowMaximumSize(int max_w, int max_h);
			void SetWindowMinimumSize(int min_w, int min_h);
			void SetWindowPosition(int x, int y);
			void SetWindowSize(int w, int h);
			void SetWindowTitle(const char* title);
			void ShowMessageBox(const SDL_MessageBoxData* data, int* buttonid);
			void ShowSimpleMessageBox(Uint32 flags, const char* title, const char* message, SDL_Window* window = nullptr);
			void ShowWindow();
			void UpdateWindowSurface();
			void UpdateWindowSurfaceRects(const SDL_Rect* rects, int numrects);
			void VideoInit(const char* driver_name);
			void VideoQuit();
		};
	}
}