#include "SurfaceManager.h"

namespace ku = GameSystem::Kureha;

SDL_Surface* ku::SurfaceManager::load(std::string path){
	if (map.find(path) != map.end()){
		map[path].cnt++;
	}
	else{
		map[path].cnt = 1;
		map[path].sur = IMG_Load(path.c_str());
		if (map[path].sur == nullptr){
			LogSystem::Log << SDL_GetError() << std::endl;
		}
	}
	return map[path].sur;
}

void ku::SurfaceManager::unload(std::string path){
	if (map.find(path) != map.end()){
		if (map[path].cnt == 1){
			SDL_FreeSurface(map[path].sur);
			map.erase(path);
		}
		else{
			map[path].cnt--;
		}
	}
}

SDL_Surface* ku::SurfaceManager::get(std::string path){
	if (map.find(path) != map.end()){
		return map[path].sur;
	}
	else{
		return nullptr;
	}
}