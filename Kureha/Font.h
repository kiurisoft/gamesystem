#pragma once

#include <SDL2\SDL.h>
#include <SDL2\SDL_ttf.h>
#include <LogSystem\Log.h>
#include <unordered_map>

namespace GameSystem{
	namespace Kureha{
		class Font{
		private:
			std::unordered_map<int, TTF_Font*> fonts;
			int cur_size;

			std::unordered_map<std::string, std::unordered_map<int, SDL_Surface*>> text_cache;
			//std::unordered_map<std::pair<std::string, int>, SDL_Surface*> text_cache;

			Font(const Font&);
			Font operator=(const Font&);
		public:
			Font();
			~Font();

			//wrapping functions
			void OpenFont(std::string path, int size);
			//OpenFontRW 
			//OpenFontIndex
			//OpenFontIndexRW
			//ByteSwappedUNICODE
			int GetFontStyle();
			void SetFontStyle(int style);
			int GetFontOutline();
			void SetFontOutline(int outline);
			int GetFontHinting();
			void SetFontHinting(int hinting);
			int GetFontKerning();
			void SetFontKerning(int allowed);
			int FontHeight();
			int FontAscent();
			int FontDescent();
			int FontLineSkip();
			int FontFaces();
			int FontFaceIsFixedWidth();
			char* FontFaceFamilyName();
			char* FontFaceStyleName();
			int GlyphIsProvided(Uint16 ch);
			void GlyphMetrics(Uint16 ch, int* minx, int* maxx, int* miny, int* maxy, int* advance);
			void SizeText(std::string text, int* w, int* h);
			//SizeUTF8
			//SizeUNICODE
			SDL_Surface* RenderText_Solid(std::string text, SDL_Color fg, int size = -1);
			SDL_Surface* RenderText_Shaded(std::string text, SDL_Color fg, SDL_Color bg, int size = -1);
			SDL_Surface* RenderText_Blended(std::string text, SDL_Color fg, int size = -1);
		};
	}
}