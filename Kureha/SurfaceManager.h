#pragma once

#include <SDL2\SDL.h>
#include <SDL2\SDL_image.h>
#include <LogSystem\Log.h>
#include <unordered_map>

namespace GameSystem{
	namespace Kureha{
		class SurfaceManager{
		private:
			SurfaceManager(){}
			SurfaceManager(const SurfaceManager&){}
			SurfaceManager& operator=(const SurfaceManager&){}

			struct surface{
				SDL_Surface* sur;
				int cnt;
			};

			std::unordered_map<std::string, surface> map;
		public:
			static SurfaceManager* get_manager(){
				static SurfaceManager singleton;
				return &singleton;
			}
			SDL_Surface* load(std::string path);
			void unload(std::string path);
			SDL_Surface* get(std::string path);
		};
	}
}

#define GSM GameSystem::Kureha::SurfaceManager::get_manager()