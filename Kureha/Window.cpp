#include "Window.h"

namespace ku = GameSystem::Kureha;

ku::Window::Window(const char* title, int x, int y,
					int w, int h, Uint32 flags) :win(nullptr){
	win = SDL_CreateWindow(title, x, y, w, h, flags);
	if (win == nullptr){
		LogSystem::Log << "Windowの初期化に失敗" << std::endl;
		LogSystem::Log << SDL_GetError() << std::endl;
	}
}

ku::Window::~Window(){
	if (win != nullptr){
		SDL_DestroyWindow(win);
	}
}

SDL_Window* ku::Window::get(){
	return win;
}

//wrapping functions

void ku::Window::DisableScreenSaver(){
	SDL_DisableScreenSaver();
}
void ku::Window::EnableScreenSaver(){
	SDL_EnableScreenSaver();
}

const char* ku::Window::GetCurrentVideoDriver(){
	return SDL_GetCurrentVideoDriver();
}

void ku::Window::GetDisplayBounds(int disp_index, SDL_Rect* rect){
	if (SDL_GetDisplayBounds(disp_index, rect) != 0){
		LogSystem::Log << SDL_GetError() << std::endl;
	}
}

const char* ku::Window::GetDisplayName(int disp_index){
	return SDL_GetDisplayName(disp_index);
}

int ku::Window::GetNumDisplayModes(int disp_index){
	auto comp = SDL_GetNumDisplayModes(disp_index);
	if (comp < 1){
		LogSystem::Log << SDL_GetError() << std::endl;
	}
	return comp;
}

int ku::Window::GetNumVideoDisplays(){
	auto comp = SDL_GetNumVideoDisplays();
	if (comp < 1){
		LogSystem::Log << SDL_GetError() << std::endl;
	}
	return comp;
}

int ku::Window::GetNumVideoDrivers(){
	auto comp = SDL_GetNumVideoDrivers();
	if (comp < 1){
		LogSystem::Log << SDL_GetError() << std::endl;
	}
	return comp;
}

const char* ku::Window::GetVideoDriver(int index){
	return SDL_GetVideoDriver(index);
}

float ku::Window::GetWindowBrightness(){
	return SDL_GetWindowBrightness(win);
}

int ku::Window::GetWindowDisplayIndex(){
	auto comp = SDL_GetWindowDisplayIndex(win);
	if (comp < 0){
		LogSystem::Log << SDL_GetError() << std::endl;
	}
	return comp;
}

Uint32 ku::Window::GetWindowFlags(){
	return SDL_GetWindowFlags(win);
}

void ku::Window::GetWindowGammaRamp(Uint16* r, Uint16* g, Uint16* b){
	if (SDL_GetWindowGammaRamp(win, r, g, b) != 0){
		LogSystem::Log << SDL_GetError() << std::endl;
	}
}

SDL_bool ku::Window::GetWindowGrab(){
	return SDL_GetWindowGrab(win);
}

int ku::Window::GetWindowID(){
	return SDL_GetWindowID(win);
}

void ku::Window::GetWindowMaximumSize(int* w, int* h){
	SDL_GetWindowMaximumSize(win, w, h);
}

void ku::Window::GetWindowMinimumSize(int* w, int* h){
	SDL_GetWindowMinimumSize(win, w, h);
}

Uint32 ku::Window::GetWindowPixelFormat(){
	auto comp = SDL_GetWindowPixelFormat(win);
	if (comp == SDL_PIXELFORMAT_UNKNOWN){
		LogSystem::Log << SDL_GetError() << std::endl;
	}
	return comp;
}

void ku::Window::GetWindowPosition(int* x, int* y){
	SDL_GetWindowPosition(win, x, y);
}

void ku::Window::GetWindowSize(int* w, int* h){
	SDL_GetWindowSize(win, w, h);
}

/*
SDL_Surface* GetWindowSurface(){
	return SDL_GetWindowSurface(win);
}
*/

const char* ku::Window::GetWindowTitle(){
	return SDL_GetWindowTitle(win);
}

void ku::Window::HideWindow(){
	SDL_HideWindow(win);
}

SDL_bool ku::Window::IsScreenSaverEnabled(){
	return SDL_IsScreenSaverEnabled();
}

void ku::Window::MaximizeWindow(){
	SDL_MaximizeWindow(win);
}

void ku::Window::MinimizeWindow(){
	SDL_MinimizeWindow(win);
}

void ku::Window::RaiseWindow(){
	SDL_RaiseWindow(win);
}

void ku::Window::RestoreWindow(){
	SDL_RestoreWindow(win);
}

void ku::Window::SetWindowBordered(SDL_bool bordered){
	SDL_SetWindowBordered(win, bordered);
}

void ku::Window::SetWindowBrightness(float brightness){
	if (SDL_SetWindowBrightness(win, brightness) != 0){
		LogSystem::Log << SDL_GetError() << std::endl;
	}
}

void ku::Window::SetWindowFullscreen(Uint32 flags){
	if (SDL_SetWindowFullscreen(win, flags) != 0){
		LogSystem::Log << SDL_GetError() << std::endl;
	}
}

void ku::Window::SetWindowGammaRamp(const Uint16* r, const Uint16* g, const Uint16* b){
	if (SDL_SetWindowGammaRamp(win, r, g, b) != 0){
		LogSystem::Log << SDL_GetError() << std::endl;
	}
}

void ku::Window::SetWindowGrab(SDL_bool grabbed){
	SDL_SetWindowGrab(win, grabbed);
}

void ku::Window::SetWindowIcon(SDL_Surface* icon){
	SDL_SetWindowIcon(win, icon);
}

void ku::Window::SetWindowMaximumSize(int max_w, int max_h){
	SDL_SetWindowMaximumSize(win, max_w, max_h);
}

void ku::Window::SetWindowMinimumSize(int min_w, int min_h){
	SDL_SetWindowMinimumSize(win, min_w, min_h);
}

void ku::Window::SetWindowPosition(int x, int y){
	SDL_SetWindowPosition(win, x, y);
}

void ku::Window::SetWindowSize(int w, int h){
	SDL_SetWindowSize(win, w, h);
}

void ku::Window::SetWindowTitle(const char* title){
	SDL_SetWindowTitle(win, title);
}

void ku::Window::ShowMessageBox(const SDL_MessageBoxData* data, int* buttonid){
	if (SDL_ShowMessageBox(data, buttonid) != 0){
		LogSystem::Log << SDL_GetError() << std::endl;
	}
}

void ku::Window::ShowSimpleMessageBox(Uint32 flags, const char* title, const char* message, SDL_Window* window){
	if (SDL_ShowSimpleMessageBox(flags, title, message, window) != 0){
		LogSystem::Log << SDL_GetError() << std::endl;
	}
}

void ku::Window::ShowWindow(){
	SDL_ShowWindow(win);
}

void ku::Window::UpdateWindowSurface(){
	if (SDL_UpdateWindowSurface(win) != 0){
		LogSystem::Log << SDL_GetError() << std::endl;
	}
}

void ku::Window::UpdateWindowSurfaceRects(const SDL_Rect* rects, int numrects){
	if (SDL_UpdateWindowSurfaceRects(win, rects, numrects) != 0){
		LogSystem::Log << SDL_GetError() << std::endl;
	}
}

void ku::Window::VideoInit(const char* driver_name){
	if (SDL_VideoInit(driver_name) != 0){
		LogSystem::Log << SDL_GetError() << std::endl;
	}
}

void ku::Window::VideoQuit(){
	SDL_VideoQuit();
}