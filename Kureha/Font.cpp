#include "Font.h"

namespace ku = GameSystem::Kureha;

ku::Font::Font() :cur_size(0){
}


ku::Font::~Font(){

}

void ku::Font::OpenFont(std::string path, int size){
	if (fonts.find(size) != fonts.end()){
		fonts[size] = TTF_OpenFont(path.c_str(), size);
	}
	cur_size = size;
	if (fonts[size] == nullptr){
		LogSystem::Log << SDL_GetError() << std::endl;
	}
}

int ku::Font::GetFontStyle(){
	return ((fonts[cur_size] == nullptr) ? -1 : TTF_GetFontStyle(fonts[cur_size]));
}

void ku::Font::SetFontStyle(int style){
	if (fonts[cur_size] != nullptr){
		TTF_SetFontStyle(fonts[cur_size], style);
	}
}

int ku::Font::GetFontOutline(){
	return ((fonts[cur_size] == nullptr) ? -1 : TTF_GetFontOutline(fonts[cur_size]));
}

void ku::Font::SetFontOutline(int outline){
	if (fonts[cur_size] != nullptr){
		TTF_SetFontOutline(fonts[cur_size], outline);
	}
}

int ku::Font::GetFontHinting(){
	return ((fonts[cur_size] == nullptr) ? -1 : TTF_GetFontHinting(fonts[cur_size]));
}

void ku::Font::SetFontHinting(int hinting){
	if (fonts[cur_size] != nullptr){
		TTF_SetFontHinting(fonts[cur_size], hinting);
	}
}

int ku::Font::GetFontKerning(){
	return ((fonts[cur_size] == nullptr) ? -1 : TTF_GetFontKerning(fonts[cur_size]));
}

void ku::Font::SetFontKerning(int allowed){
	if (fonts[cur_size] != nullptr){
		TTF_SetFontKerning(fonts[cur_size], allowed);
	}
}

int ku::Font::FontHeight(){
	return ((fonts[cur_size] == nullptr) ? -1 : TTF_FontHeight(fonts[cur_size]));
}

int ku::Font::FontAscent(){
	return ((fonts[cur_size] == nullptr) ? -1 : TTF_FontAscent(fonts[cur_size]));
}

int ku::Font::FontDescent(){
	return ((fonts[cur_size] == nullptr) ? -1 : TTF_FontDescent(fonts[cur_size]));
}

int ku::Font::FontLineSkip(){
	return ((fonts[cur_size] == nullptr) ? -1 : TTF_FontLineSkip(fonts[cur_size]));
}

int ku::Font::FontFaces(){
	return ((fonts[cur_size] == nullptr) ? -1 : TTF_FontFaces(fonts[cur_size]));
}

int ku::Font::FontFaceIsFixedWidth(){
	return ((fonts[cur_size] == nullptr) ? -1 : TTF_FontFaceIsFixedWidth(fonts[cur_size]));
}

char* ku::Font::FontFaceFamilyName(){
	return ((fonts[cur_size] == nullptr) ? nullptr : TTF_FontFaceFamilyName(fonts[cur_size]));
}

char* ku::Font::FontFaceStyleName(){
	return ((fonts[cur_size] == nullptr) ? nullptr : TTF_FontFaceStyleName(fonts[cur_size]));
}

int ku::Font::GlyphIsProvided(Uint16 ch){
	auto comp = TTF_GlyphIsProvided(fonts[cur_size], ch);
	if (comp == 0){
		LogSystem::Log << ch << " is unloaded." << std::endl;
	}
	return comp;
}

void ku::Font::GlyphMetrics(Uint16 ch, int* minx, int* maxx, int* miny, int* maxy, int* advance){
	if (TTF_GlyphMetrics(fonts[cur_size], ch, minx, maxx, miny, maxy, advance) == -1){
		LogSystem::Log << SDL_GetError() << std::endl;
	}
}

void ku::Font::SizeText(std::string text, int* w, int* h){
	if (TTF_SizeText(fonts[cur_size], text.c_str(), w, h) == -1){
		LogSystem::Log << SDL_GetError() << std::endl;
	}
}

SDL_Surface* ku::Font::RenderText_Solid(std::string text, SDL_Color fg, int size){
	auto size_ = ((size == -1) ? cur_size : ((fonts.find(size) == fonts.end()) ? -1 : size));
	if (size_ == -1){
		LogSystem::Log << "the font size is not founded." << std::endl;
		return nullptr;
	}
	if (text_cache.find(text) == text_cache.end() | 
		text_cache[text].find(size_) == text_cache[text].end()){
		text_cache[text][size_] = TTF_RenderText_Solid(fonts[size_], text.c_str(), fg);
	}
	return text_cache[text][size_];
}

SDL_Surface* ku::Font::RenderText_Shaded(std::string text, SDL_Color fg, SDL_Color bg, int size){
	auto size_ = ((size == -1) ? cur_size : ((fonts.find(size) == fonts.end()) ? -1 : size));
	if (size_ == -1){
		LogSystem::Log << "the font size is not founded." << std::endl;
		return nullptr;
	}
	if (text_cache.find(text) == text_cache.end() |
		text_cache[text].find(size_) == text_cache[text].end()){
		text_cache[text][size_] = TTF_RenderText_Shaded(fonts[size_], text.c_str(), fg, bg);
	}
	return text_cache[text][size_];
}

SDL_Surface* ku::Font::RenderText_Blended(std::string text, SDL_Color fg, int size){
	auto size_ = ((size == -1) ? cur_size : ((fonts.find(size) == fonts.end()) ? -1 : size));
	if (size_ == -1){
		LogSystem::Log << "the font size is not founded." << std::endl;
		return nullptr;
	}
	if (text_cache.find(text) == text_cache.end() |
		text_cache[text].find(size_) == text_cache[text].end()){
		text_cache[text][size_] = TTF_RenderText_Blended(fonts[size_], text.c_str(), fg);
	}
	return text_cache[text][size_];
}