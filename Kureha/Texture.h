#pragma once

#include <SDL2\SDL.h>
#include <LogSystem\Log.h>

namespace GameSystem{
	namespace Kureha{
		class Texture{
		private:
			SDL_Texture* texture;
			
		public:
			Texture();
			~Texture();

			SDL_Texture* get();
			void GetTextureAlphaMod(Uint8* alpha);
			void GetTextureBlendMode(SDL_BlendMode* blendmode);
			void GetTextureColorMod(Uint8* r, Uint8* g, Uint8* b);
			//void LockTexture(const SDL_Rect* rect, void** pixels, int* pitch);
			void QueryTexture(Uint32* format, int* access, int* w, int* h);
			void SetTextureAlphaMod(Uint8 alpha);
			void SetTextureBlendMode(SDL_BlendMode blendmode);
			void SetTextureColorMod(Uint8 r, Uint8 g, Uint8 b);
			//void UnlockTexture();
			//void UpdateTexture(const SDL_Rect* rect, void** pixels, int* pitch);
			//void UpdateYUVTexture(const SDL_Rect* rect, );
		};
	}
}