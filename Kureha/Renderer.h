#pragma once

#include <SDL2\SDL.h>
#include <LogSystem\Log.h>

#include "Texture.h"

namespace GameSystem{
	namespace Kureha{
		class Renderer{
		private:
			SDL_Renderer* ren;

			Renderer(const Renderer&);
			Renderer& operator=(const Renderer&);
		public:
			Renderer(SDL_Window* win);
			~Renderer();

			//wrapping functions
			int GetNumRenderDrivers();
			void GetRenderDrawBlendMode(SDL_BlendMode* blendmode);
			void GetRenderDrawColor(Uint8* r, Uint8* g, Uint8* b, Uint8* a);
			//GetRenderDriverInfo
			SDL_Texture* GetRenderTarget();
			//GetRenderer
			//GetRendererInfo
			void GetRendererOutputSize(int* w, int* h);
			void RenderClear();
			void RenderCopy(Texture& texture, const SDL_Rect* src, const SDL_Rect* dst);
			void RenderCopyEx(Texture& texture, const SDL_Rect* src, const SDL_Rect* dst,
				const double angle, const SDL_Point* center, const SDL_RendererFlip flip);
			void RenderDrawLine(int x1, int y1, int x2, int y2);
			void RenderDrawLines(const SDL_Point* points, int count);
			void RenderDrawPoint(int x, int y);
			void RenderDrawPoints(const SDL_Point* points, int count);
			void RenderDrawRect(const SDL_Rect* rect);
			void RenderDrawRects(const SDL_Rect* rects, int count);
			void RenderFillRect(const SDL_Rect* rect);
			void RenderFillRects(const SDL_Rect* rects, int count);
			void RenderGetClipRect(SDL_Rect* rect);
			void RenderGetLogicalSize(int* w, int* h);
			void RenderGetScale(float* scaleX, float* scaleY);
			void RenderGetViewport(SDL_Rect* rect);
			void RenderPresent();
			//RenderReadPixels
			void RenderSetClipRect(const SDL_Rect* rect);
			void RenderSetLogicalSize(int w, int h);
			void RenderSetScale(float scaleX, float scaleY);
			void RenderSetViewport(const SDL_Rect* rect);
			//RenderTargetSupported
			void SetRenderDrawBlendMode(SDL_BlendMode blendmode);
			void SetRenderDrawColor(Uint8 r, Uint8 g, Uint8 b, Uint8 a);
			void SetRenderTarget(Texture& texture);
		};
	}
}