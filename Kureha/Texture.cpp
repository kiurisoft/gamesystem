#include "Texture.h"

namespace ku = GameSystem::Kureha;

ku::Texture::Texture(){

}

ku::Texture::~Texture(){
	if (texture != nullptr){
		SDL_DestroyTexture(texture);
	}
}

SDL_Texture* ku::Texture::get(){
	return texture;
}

void ku::Texture::GetTextureAlphaMod(Uint8* alpha){
	if (SDL_GetTextureAlphaMod(texture, alpha) != 0){
		LogSystem::Log << SDL_GetError() << std::endl;
	}
}

void ku::Texture::GetTextureBlendMode(SDL_BlendMode* blendmode){
	if (SDL_GetTextureBlendMode(texture, blendmode) != 0){
		LogSystem::Log << SDL_GetError() << std::endl;
	}
}

void ku::Texture::GetTextureColorMod(Uint8* r, Uint8* g, Uint8* b){
	if (SDL_GetTextureColorMod(texture, r, g, b) != 0){
		LogSystem::Log << SDL_GetError() << std::endl;
	}
}

void ku::Texture::QueryTexture(Uint32* format, int* access, int* w, int* h){
	if (SDL_QueryTexture(texture, format, access, w, h) != 0){
		LogSystem::Log << SDL_GetError() << std::endl;
	}
}

void ku::Texture::SetTextureAlphaMod(Uint8 alpha){
	if (SDL_SetTextureAlphaMod(texture, alpha) != 0){
		LogSystem::Log << SDL_GetError() << std::endl;
	}
}

void ku::Texture::SetTextureBlendMode(SDL_BlendMode blendmode){
	if (SDL_SetTextureBlendMode(texture, blendmode) != 0){
		LogSystem::Log << SDL_GetError() << std::endl;
	}
}

void ku::Texture::SetTextureColorMod(Uint8 r, Uint8 g, Uint8 b){
	if (SDL_SetTextureColorMod(texture, r, g, b) != 0){
		LogSystem::Log << SDL_GetError() << std::endl;
	}
}

